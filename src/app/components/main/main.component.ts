import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common.service';
import { Router, RouterModule } from '@angular/router';

import { Data } from '../../providers/data';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  propiedades: any[] = [];
  propiedadSeleccionada : any[] = [];

  constructor( private service:CommonService, private router: Router, private data: Data ) {
    this.service.getProperties()
      .subscribe( (respuesta:any) => {
        this.propiedades = respuesta;
        // console.log( respuesta );
      });
  }


  getDetails( propiedad ){

    this.data.propiedadSeleccionada = propiedad;

    // this.propiedadSeleccionada = propiedad;

    this.router.navigate(['/propiedad', propiedad.id])


    // console.log( this.propiedadSeleccionada );

  }

  ngOnInit() {
  }

}
