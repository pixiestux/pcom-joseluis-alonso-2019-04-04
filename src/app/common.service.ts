import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor( private http:HttpClient ) {
    console.log("Properties services ready");
  }

  getProperties(){
    const URL = 'https://api.myjson.com/bins/11tqfk';
    return this.http.get(URL);
  }
}
